import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('../components/Home.vue')
    },
    {
      path: '/store/composition',
      name: 'Store (composition)',
      component: () => import('../components/store/Composition.vue')
    },
    {
      path: '/store/options',
      name: 'Store (options)',
      component: () => import('../components/store/Options.vue')
    },
    {
      path: '/route',
      name: 'Route instructions',
      component: () => import('../components/RouterParam.vue')
    },
    {
      path: '/route/:number',
      name: 'Route parameters',
      component: () => import('../components/RouterParam.vue')
    }
  ]
})

export default router

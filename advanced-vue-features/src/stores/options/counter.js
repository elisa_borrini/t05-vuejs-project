import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', {
  state: () => ({
    count: 0
  }),
  getters: {
    doubleCount: (state) => state.count * 2
  },
  actions: {
    randomize(max = 100) {
      this.count = Math.floor(Math.random() * max)
    }
  }
})

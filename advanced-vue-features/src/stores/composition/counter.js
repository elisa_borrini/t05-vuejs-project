import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function randomize(max = 100) {
    count.value = Math.floor(Math.random() * max)
  }

  return { count, doubleCount, randomize }
})

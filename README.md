# T05 - VueJS project

This repository contains two folders:
- [advanced-vue-features](https://gitlab.com/i3lab/hypermedia/hypermedia-22-23/t05-vuejs-project/-/tree/master/advanced-vue-features) contains an application showcasing the vue features presented at the beginning of the lecture of 30/03/2023.
- [vue-project](https://gitlab.com/i3lab/hypermedia/hypermedia-22-23/t05-vuejs-project/-/tree/master/vue-project) contains the code developed during the hands-on session on the same day.